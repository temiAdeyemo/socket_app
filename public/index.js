let newUserPass = prompt("App locked, Please input App Password.").toLowerCase();

let socket;
if (newUserPass == "adesola" ||newUserPass == "the_owner") {
  alert("Welcome in, Enjoy your sesion.....");
  socket = io.connect("http://localhost:1010/");
} else {  
  alert("Wrong Password");
  const chat = document.getElementById("mario-chat");
  chat.innerHTML =
  "<p><strong>Please get a valid password and reload.</strong></p>"
  chat.style.background = "#e0b0b0";
}

const message = document.getElementById("message");
const handle = document.getElementById("handle");
const btn = document.getElementById("send");
const output = document.getElementById("output");
const feedback = document.getElementById("feedback");

btn.addEventListener("click", () => {

  if (handle.value == null || handle.value == "") {
    alert("Pls input a handle");
    return null;
  }
  
  if (message.value == null || message.value == "") {
    alert("Cannot send blank message!");
    return null;
  }

  socket.emit("chat", {
    message: message.value,
    handle: handle.value,
  });
  message.value = "";
});

message.addEventListener("keypress", () => {
  socket.emit("typing", handle.value);
});

socket.on("chat", (data) => {
  feedback.innerHTML = "";
  output.innerHTML +=
    "<p><strong>" + data.handle + ":</strong>" + data.message + "</p>";
});

socket.on("typing", (data) => {
  feedback.innerHTML = "<p><em>" + data + " is typing a message...</em></p>";
});
